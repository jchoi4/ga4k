terraform {
  required_providers {
    graphql = {
      source  = "sullivtr/graphql"
      version = "~>1.5.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.8.0"
    }
  }
}

